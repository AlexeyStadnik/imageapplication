<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<h1>WEB project</h1>

<form id="form2" method="post" action="/image-web/images"
	enctype="multipart/form-data">
	<input name="file2" id="file2" type="file" />
</form>

<button value="Submit" onclick="RestPost()">Send file</button>

<button type='button' onclick='RestGet()'>Get all items</button>


<div id="allPictures"></div>
<script>
	var prefix = '/helloproject';

	function RestPost() {

		var oMyForm = new FormData();
		oMyForm.append("file", file2.files[0]);

		$.ajax({
			type : 'POST',
			url : prefix + '/images',
			data : oMyForm,
			dataType : 'text',
			processData : false,
			contentType : false,
			success : function(data) {
				RestGet();
			},
			error : function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.status + ' ' + jqXHR.responseText);
			}
		});
	}

	var RestGet = function() {
		$
				.ajax({
					type : 'GET',
					url : prefix + '/images',
					dataType : 'json',
					async : true,
					success : function(result) {
						var respContent = "";
						$
								.each(
										result,
										function(i, item) {
											respContent += "<button type='button' onclick='RestDelete(this.value)' value='"
													+ item
													+ "'>DELETE</button>"
											respContent += "<img src='/helloproject/images/" + item + "'/></td><br>";
										});
						$("#allPictures").html(respContent);
					},
					error : function(jqXHR, textStatus, errorThrown) {
						alert(jqXHR.status + ' ' + jqXHR.responseText);
					}
				});
	}

	var RestDelete = function(value) {
		$.ajax({
			type : 'DELETE',
			url : prefix + '/images/' + value,
			async : true,
			success : function() {
				RestGet();
			},
			error : function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.status + ' ' + jqXHR.responseText);
			}
		});
	}
</script>


