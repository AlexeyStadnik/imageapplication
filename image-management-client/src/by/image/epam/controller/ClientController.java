package by.image.epam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "client" )  
public class ClientController {
	
	@RequestMapping(method = RequestMethod.GET)
	public String startPage() {
		return "client";
	}

}

