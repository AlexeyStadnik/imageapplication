package by.image.epam.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
@RequestMapping(value = "images" )  
public class StartController {
	
	public static final String URI ="http://localhost:8080/image-gallery/images";

	private final static Logger logger = LoggerFactory.getLogger(ClientController.class);

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody String startPage() {

		RestTemplate restTemplate = new RestTemplate();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptableMediaTypes);
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<String> result = restTemplate.exchange(URI, 
				HttpMethod.GET, entity, String.class);

		logger.debug(result.getBody());
		return result.getBody();
	}

	@RequestMapping(value="/{imageTitle}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable("imageTitle") String imageTitle) {

		RestTemplate restTemplate = new RestTemplate();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.IMAGE_JPEG);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_JPEG);
		headers.setAccept(acceptableMediaTypes);
		HttpEntity<byte[]> entity = new HttpEntity<byte[]>(headers);

		ResponseEntity<byte[]> result = restTemplate.exchange(URI +"/{imageTitle}" , 
				HttpMethod.GET, entity, byte[].class,imageTitle);

		logger.debug("Getting image with title - " + imageTitle);
		return result;
	}

	@RequestMapping(value="/{imageTitle}" , method = RequestMethod.DELETE)
	public @ResponseBody String deleteImage(@PathVariable("imageTitle") String imageTitle,
			HttpServletResponse response) {

		RestTemplate restTemplate = new RestTemplate();
		List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
		acceptableMediaTypes.add(MediaType.APPLICATION_JSON);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(acceptableMediaTypes);
		HttpEntity<String> entity = new HttpEntity<String>(headers);

		ResponseEntity<String> result = restTemplate.exchange(URI +"/{imageTitle}", 
				HttpMethod.DELETE, entity, String.class,imageTitle);

		logger.debug("Deleting image with title - " + imageTitle +"\n" + "Result - " + result.getBody());
		return result.getBody();

	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String addImageFromForm(MultipartHttpServletRequest request,HttpServletResponse response) {
		Iterator<String> iterator = request.getFileNames();
		MultipartFile multiFile = null;
		if(iterator.hasNext()) {
			multiFile = request.getFile(iterator.next());
		} else {
			logger.error("Cant get data aut of multiFile, please check uploaded data");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return "Cant get data aut of multiFile, please check uploaded data";
		}
		RestTemplate restTemplate = new RestTemplate();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		ByteArrayResource contentsAsResource = null;
		try {
			contentsAsResource = new ByteArrayResource(multiFile.getBytes()){
			            @Override
			            public String getFilename(){
			                return "fileName";
			            }
			        };
		} catch (IOException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			logger.error("IOException while getting bytes out of image",e);
			return "IOException while getting bytes out of image, please check uploaded data";
		}
		map.add("file", contentsAsResource);
		String result = restTemplate.postForObject(URI, map, String.class);
		logger.debug("Posting image");
		return result;
	}


}
