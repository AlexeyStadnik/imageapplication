package by.image.epam.dao.hibernateImpl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.aop.framework.Advised;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;
import by.image.epam.service.ServiceException;

@ContextConfiguration(locations = { "classpath:/applicationContextTest.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class HibernateImageDAOImplTest {

  @Mock
  SessionFactory sessionFactory;
  @Mock
  Session session;

  @Autowired
  @InjectMocks
  ImageDAO imageDAO;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    assertNotNull(imageDAO);
  }

  @Test
  public void createImageTest() throws ServiceException, DAOException {

    when(sessionFactory.getCurrentSession()).thenReturn(session);
    Image image = mock(Image.class);
    imageDAO.createImage(image);

    verify(session, times(1)).persist(image);
    verifyNoMoreInteractions(session);
  }

  @Test
  public void deleteImageTest() throws ServiceException, DAOException {
    when(sessionFactory.getCurrentSession()).thenReturn(session);
    String imageTitle = "deletedTitle";
    imageDAO.deleteImage(imageTitle);

    verify(session, times(1)).delete(any());
    verifyNoMoreInteractions(session);

  }

  @Test
  public void getImageTest() throws ServiceException, DAOException {
    when(sessionFactory.getCurrentSession()).thenReturn(session);
    String imageTitle = null;
    imageDAO.getImage(imageTitle);

    verify(session, times(1)).get(Image.class, imageTitle);
    verifyNoMoreInteractions(session);
  }

  @Test
  public void getAllImagesTest() throws DAOException, ServiceException {
    when(sessionFactory.getCurrentSession()).thenReturn(session);
    Query mockQuery = mock(Query.class);
    when(session.createQuery(anyString())).thenReturn(mockQuery);
    when(mockQuery.list()).thenReturn(null);
    imageDAO.getAllImageTitles();
   
    verify(session, times(1)).createQuery(anyString());
    verify(mockQuery, times(1)).list();
    verifyNoMoreInteractions(session);
    verifyNoMoreInteractions(mockQuery);
  }

  

}
