package by.image.epam.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;
import by.image.epam.service.ImageService;
import by.image.epam.service.ServiceException;

@ContextConfiguration(locations = {"classpath:/applicationContextTest.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class ImageServiceImplTest {

	@Mock
	ImageDAO imageDAO;

	@InjectMocks
	@Autowired
	ImageService imageService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		assertNotNull(imageService);
	}

	@Test
	public void createImageTest() throws ServiceException, DAOException {

		Image mockImage = mock(Image.class);
		imageService.createImage(mockImage);

		verify(imageDAO,times(1)).createImage(mockImage);
		verifyNoMoreInteractions(imageDAO);
	}

	@Test
	public void deleteImageTest() throws ServiceException, DAOException {
		String imageTitle = null;
		imageService.deleteImage(imageTitle);

		verify(imageDAO,times(1)).deleteImage(imageTitle);
		verifyNoMoreInteractions(imageDAO);

	}

	@Test
	public void getImageTest() throws ServiceException, DAOException  {
		String imageTitle = null;
		imageService.getImage(imageTitle);

		verify(imageDAO,times(1)).getImage(imageTitle);
		verifyNoMoreInteractions(imageDAO);
	}
	
	@Test
	public void getAllImagesTest() throws DAOException, ServiceException {
		imageService.getAllImageTitles();
		
		verify(imageDAO,times(1)).getAllImageTitles();
		verifyNoMoreInteractions(imageDAO);
	}
}
