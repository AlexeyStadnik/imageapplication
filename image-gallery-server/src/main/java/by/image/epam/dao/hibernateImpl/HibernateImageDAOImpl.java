package by.image.epam.dao.hibernateImpl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;

@Transactional(rollbackFor = Exception.class)
public class HibernateImageDAOImpl implements ImageDAO {

  private static final Logger LOGGER = LoggerFactory.getLogger(HibernateImageDAOImpl.class);

  private SessionFactory sessionFactory;

  public HibernateImageDAOImpl() {

  }

  public HibernateImageDAOImpl(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public SessionFactory getSessionFactory() {
    return sessionFactory;
  }

  public void setSessionFactory(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public String createImage(Image image) throws DAOException {
    LOGGER.debug("Creating image");
    Session session = sessionFactory.getCurrentSession();
    try {
      session.persist(image);
    } catch (HibernateException e) {
      throw new DAOException("Exception while image with title - " 
    + image.getImageTitle() + " creation process ", e);
    }
    return image.getImageTitle();
  }

  /**
   * 
   */
  public Image getImage(String imageTitle) throws DAOException {
    LOGGER.debug("Getting image");
    Session session = sessionFactory.getCurrentSession();
    Image image = null;
    try {
      image = (Image) session.get(Image.class, imageTitle);
    } catch (HibernateException e) {
      throw new DAOException("Exception while image with title -  " + image.getImageTitle() + " reading process ", e);
    }
    return image;
  }

  public void deleteImage(String imageTitle) throws DAOException {
    LOGGER.debug("Deleting image");
    Session session = sessionFactory.getCurrentSession();
    Image image = new Image();
    image.setImageTitle(imageTitle);
    try {
      session.delete(image);
    } catch (HibernateException e) {
      throw new DAOException("Exception while deleting image process.", e);
    }
  }

  public String updateImage(Image image) throws DAOException {
    throw new UnsupportedOperationException("This operation is not supported");
  }

  public List<String> getAllImageTitles() throws DAOException {
    LOGGER.debug("Retrieving all titles");
    Session session = sessionFactory.getCurrentSession();
    Query query = null;
    try {
      query = session.createQuery("SELECT imageTitle FROM  Image");
    } catch (HibernateException e) {
      throw new DAOException("Exception while getting all  titles  process.", e);
    }
    return query.list();
  }
}
