package by.image.epam.dao.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import by.image.epam.dao.DAOException;
import by.image.epam.dao.ImageDAO;
import by.image.epam.entity.Image;

public class ImageDAOImpl implements ImageDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ImageDAOImpl.class);
	
	public static final String PATH = ResourceBundle.getBundle("properties").getString("path");

	public String createImage(Image image) throws DAOException {
		LOGGER.debug("begin creating an image");
		try {
			File file = new File(PATH + image.getImageTitle() + ".jpg");
			FileUtils.writeByteArrayToFile(file, image.getImageData());
		} catch (IOException e) {
			throw new DAOException("Exception while image with title -  "
					+  image.getImageTitle() +" creation process ",e);
		}
		return image.getImageTitle();
	}

	public Image getImage(String imageTitle) throws DAOException {
		LOGGER.debug("getting an image");
		File file = new File(PATH + imageTitle + ".jpg");
		Image image = new Image();
		image.setImageTitle(imageTitle);
		try {
			image.setImageData(FileUtils.readFileToByteArray(file));
		} catch (IOException e) {
			throw new DAOException("Exception while image with title -  "
					+  image.getImageTitle() +" reading process ",e);
		}
		return image;
	}

	public void deleteImage(String imageTitle) throws DAOException {
		LOGGER.debug("deleting an image");
		File file = new File(PATH + imageTitle + ".jpg");
		FileUtils.deleteQuietly(file);
	}

	public String updateImage(Image image) throws DAOException {
		throw new UnsupportedOperationException("This operation is not supported");
	}

	public List<String> getAllImageTitles() throws DAOException {
		LOGGER.debug("getting all images");
		File folder = new File(PATH);
		File[] listOfFiles = folder.listFiles();
		List<String> titleList = new ArrayList<String>();
		for (File file : listOfFiles) {
		    if (file.isFile()) {
		        titleList.add(file.getName());
		    }
		}
		return titleList;
	}

}

