package by.image.epam.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IMAGES")
public class Image  implements Serializable{

	private static final long serialVersionUID = -8640697573116760467L;

	@Id
	@Column(name = "IMAGE_TITLE")
	private String imageTitle;

	@Column(name = "IMAGE_DATA")
	private byte [] imageData;

	public Image() {

	}


	public Image(String imageTitle, byte[] imageData) {
		super();
		this.imageTitle = imageTitle;
		this.imageData = imageData;
	}
	public String getImageTitle() {
		return imageTitle;
	}
	public void setImageTitle(String imageTitle) {
		this.imageTitle = imageTitle;
	}
	public byte[] getImageData() {
		return imageData;
	}
	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(imageData);
		result = prime * result
				+ ((imageTitle == null) ? 0 : imageTitle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Image other = (Image) obj;
		if (!Arrays.equals(imageData, other.imageData))
			return false;
		if (imageTitle == null) {
			if (other.imageTitle != null)
				return false;
		} else if (!imageTitle.equals(other.imageTitle))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Image [imageTitle=" + imageTitle + "]";
	}

}