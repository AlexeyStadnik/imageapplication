package by.image.epam.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import by.image.epam.entity.Image;
import by.image.epam.service.ImageService;
import by.image.epam.service.ServiceException;
import by.image.epam.util.BadRequestException;
import by.image.epam.util.ErrorMessage;

@Controller
@RequestMapping(value = "images" )  
public class ImageController {

	@Autowired
	private ImageService imageService;

	@RequestMapping(method = RequestMethod.GET)
	public  @ResponseBody List<String> getAllImages(HttpServletResponse response) {
		List<String> titleList = null;
		try {
			titleList = imageService.getAllImageTitles();
		} catch (ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return Arrays.asList("ServiceException while getting list of titles");
		}
		return titleList;
	}

	@RequestMapping(value="/{imageTitle}" , method = RequestMethod.DELETE)
	public @ResponseBody String deleteImage(@PathVariable("imageTitle") String imageTitle,
			HttpServletResponse response) {
		try {
			imageService.deleteImage(imageTitle);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return "ServiceException while deleting image with id - " + imageTitle;
		}
		return "delete";
	}

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String addImageFromForm(MultipartHttpServletRequest request,HttpServletResponse response) {
		MultipartFile multiFile = null;
		multiFile = request.getFile("file");
		if(multiFile == null ) {
			throw new BadRequestException("Exception while getting MultipartFile from the request(null getting)");
		}
		try {
			Image newImage = new Image();
			newImage.setImageTitle("title" + System.currentTimeMillis());
			newImage.setImageData(multiFile.getBytes());
			imageService.createImage(newImage);
		} catch (ServiceException e) {
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return "ServiceException while image saving proces. Exception message - " + e.getMessage();
		} catch (IOException e) {
			throw new BadRequestException("IOException while getting MultipartFile from the request",e);
		}
		response.setStatus(HttpServletResponse.SC_CREATED);
		return "Image has been created";
	}


	@RequestMapping(value="/{imageTitle}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImage(@PathVariable("imageTitle") String imageTitle,HttpServletResponse response)  {
		Image image = null;
		byte [] imageData = null;
		HttpHeaders headers = new HttpHeaders();
		try {
			image = imageService.getImage(imageTitle);
			if(image == null) {
				throw new BadRequestException("There are no image with id - " + imageTitle);
			}
			imageData = image.getImageData();
			headers.setContentType(MediaType.IMAGE_JPEG);
		} catch (ServiceException e) {
			return new ResponseEntity<byte[]>(new byte[0], HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<byte[]>(imageData, headers, HttpStatus.CREATED);
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ErrorMessage handleError(HttpServletResponse response,Exception exception) {
		response.setStatus(400);
		ErrorMessage errorMessage = new ErrorMessage(400,exception.getMessage());
		return errorMessage;
	}

}

