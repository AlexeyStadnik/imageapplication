package by.image.epam.util;

import java.io.Serializable;

public class ErrorMessage implements Serializable {

	private static final long serialVersionUID = 5483429525742428114L;

	private final int status;
	private final String message;

	public ErrorMessage(int s, String m) {
		status = s;
		message = m;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}
}
